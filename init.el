;; init.el -- Bootstrap to emacs configs -*- lexical-binding: t; -*-
;;
;;; Commentary:

;;; Code:

(add-to-list 'load-path "~/.emacs.d/customizations")

(load "init-package.el")
(load "init-ui.el")
(load "init-key-bindings.el")
(load "init-misc.el")
(load "init-modeline.el")
(load "init-navigation.el")
(load "init-editing.el")
(load "init-search.el")
(load "init-source-control.el")
(load "init-evil.el")
(load "init-org.el")
;; (load "init-language-server.el")
(load "init-note-taking.el")

;; Language-specific
(load "init-clojure.el")
(load "init-data-analysis.el")
(load "init-elixir.el")
(load "init-go.el")
(load "init-haskell.el")
(load "init-js.el")
(load "init-lisp.el")
(load "init-markdown.el")
(load "init-python.el")
(load "init-ruby.el")
; (load "init-rust.el")
; (load "init-scala.el")

(require 'loadhist)
(file-dependents (feature-file 'cl))

(require 'straight-x)
(straight-x-clean-unused-repos)

(require 'server)
(unless (server-running-p)
  (server-start))

(defun load-ui (frame)
  "Ensure ui configs are loaded when opens in FRAME."
  (with-selected-frame frame
                       (load "init-ui.el")))

(add-hook 'after-make-frame-functions 'load-ui)


(provide 'init)

;;; init.el ends here
