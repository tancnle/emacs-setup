# My personal Emacs config

This is my personal emacs config, adapted from
[flyingmachine](https://github.com/flyingmachine/emacs.d/)

# Other inspirations
- Doom Emacs (https://github.com/hlissner/doom-emacs)
- Spacemacs (https://www.spacemacs.org/)
