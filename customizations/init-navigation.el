;; init-navigation.el -- navigation-related packages/configs -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; These customizations make it easier for you to navigate files,
;; switch buffers, and choose options from the minibuffer.
;;
;; ace-window
;; dired
;; diredfl
;; eyebrowse
;; uniquify
;; recentf
;; projectile
;; projectile-rails
;; winum

;;; Code:

(use-package ace-window)

(use-package dired
  :straight nil
  :config
  (progn
    (setq dired-use-ls-dired nil         ; Remove ls warning on dired
          dired-auto-revert-buffer t     ; Don't prompt to revert
          dired-recursive-copies 'always ; Always copy recursively
          dired-recursive-deletes 'top   ; Always delete recursively
          dired-dwim-target t)           ; Copy between dired in different buffers

    ;; Avoid creating uneeded intermediate buffers
    (put 'dired-find-alternate-file 'disabled nil)))

;; Colourful dired
(use-package diredfl
  :commands diredfl-global-mode
  :hook (dired-mode . diredfl-mode))

(use-package eyebrowse
  :config
  (setq eyebrowse-new-workspace t)
  (eyebrowse-mode t))

;; "When several buffers visit identically-named files,
;; Emacs must give the buffers distinct names. The usual method
;; for making buffer names unique adds ‘<2>’, ‘<3>’, etc. to the end
;; of the buffer names (all but one of them).
;; The forward naming method includes part of the file's directory
;; name at the beginning of the buffer name
;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Uniquify.html
(use-package uniquify
  :straight nil
  :config
  (setq uniquify-buffer-name-style 'forward))

(use-package recentf
  :init
  (setq recentf-save-file (concat user-emacs-directory ".recentf")
        recentf-max-saved-items 500
        recentf-max-menu-items 40
        recentf-auto-cleanup 'never)
  :config
  (recentf-mode +1))

(use-package projectile
  :diminish projectile-mode " Ⓟ"
  :init
  (progn
    (setq projectile-sort-order 'recentf
          projectile-globally-ignored-files '(".DS_Store" "Icon" "TAGS")
          projectile-globally-ignored-file-suffixes '(".elc" ".pyc" ".o")
          projectile-enable-caching t))
  :config
  (projectile-mode +1))

(use-package projectile-rails
  :defer t
  :init
  (add-hook 'enh-ruby-mode-hook 'projectile-rails-on))

(use-package shackle
  :config
  (setq shackle-default-alignment 'below
        shackle-default-size 8
        shackle-rules
        '(("*eshell*" :select t :frame t :other t)
          ))

  (shackle-mode 1))

(use-package winum
  :config
  (progn
    (setq winum-auto-setup-mode-line nil)
    (winum-mode)))

(provide 'init-navigation)
;;; init-navigation.el ends here
