;; init-lisp.el -- lisp-related packages/configs -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; elisp-mode
;; lisp-mode

;;; Code:

(use-package elisp-mode
  :straight nil
  :defer t
  :delight emacs-lisp-mode (all-the-icons-fileicon "elisp" :v-adjust -0.09)
  :init
  (add-hook 'emacs-lisp-mode-hook #'paredit-mode))

(use-package lisp-mode
  :straight nil
  :defer t
  :delight lisp-interaction-mode (all-the-icons-fileicon "lisp" :v-adjust -0.1)
  :init
  (add-hook 'lisp-interaction-mode-hook #'eldoc-mode)
  (add-hook 'lisp-interaction-mode-hook #'paredit-mode)
  (add-hook 'lisp-mode-hook #'paredit-mode)
  (add-hook 'lisp-mode-hook #'rainbow-delimiters-mode))

(use-package paredit
  :config
  (progn
    "Turn on pseudo-structural editing of Lisp code."
    (autoload 'enable-paredit-mode "paredit" t)
    (add-hook 'eval-expression-minibuffer-setup-hook #'paredit-mode)
    (add-hook 'ielm-mode-hook #'paredit-mode)
    (add-hook 'scheme-mode-hook #'paredit-mode)))

(provide 'init-lisp)
;;; init-lisp.el ends here
