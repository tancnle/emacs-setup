;; init-key-bindings.el -- all key bindings -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; general

;;; Code:

(use-package general)

;; * Prefix Keybindings
;; :prefix can be used to prevent redundant specification of prefix keys
;; again, variables are not necessary and likely not useful if you are only
;; using a definer created with `general-create-definer' for the prefixes
;; (defconst my-leader "SPC")
;; (defconst my-local-leader "SPC m")

(general-create-definer my-leader-def
  ;; :prefix my-leader
  :prefix ",")

(general-create-definer my-local-leader-def
  ;; :prefix my-local-leader
  :prefix ", m")

;; ** Global Keybindings
(my-leader-def
  :states 'normal
  :keymaps 'override
  ","  'other-window
  "."  'mode-line-other-buffer
  ":"  'eval-expression
  "B"  'magit-blame
  "a"  'org-agenda
  "b"  'counsel-bookmark
  "c"  'org-capture
  "f"  'counsel-fzf
  "g"  'magit-status
  "p"  'counsel-projectile-switch-project
  "s"  'counsel-rg-at-point
  "w"  'save-buffer

  "dj" 'dired-jump ; open dired from current file

  "xo" 'ace-window

  "rs" 'rspec-verify-single
  "rt" 'rspec-verify
  "rl" 'rspec-rerun

  ;; Worksplace switching
  "1" 'eyebrowse-switch-to-window-config-1
  "2" 'eyebrowse-switch-to-window-config-2
  "3" 'eyebrowse-switch-to-window-config-3
  "4" 'eyebrowse-switch-to-window-config-4
  "5" 'eyebrowse-switch-to-window-config-5)

(general-define-key
  :states 'normal
  :keymaps 'override
  "C-]" 'counsel-etags-find-tag-at-point)

;; Show a list of buffers
(general-define-key
  "C-x C-b" 'ibuffer)

;; Commenting with evil-nerd-commenter
(general-define-key
  :states 'normal
  :keymaps 'override
  "gc" 'evilnc-comment-operator)

;; Helpful overrides
(general-define-key
  :states 'normal
  :keymaps 'override
  ;; Default Emacs help keybindings drop-in replacement
  "C-h f" 'helpful-callable
  "C-h v" 'helpful-variable
  "C-h k" 'helpful-key

  "C-c C-d" 'helpful-at-point
  "C-h F"   'helpful-function
  "C-h C"   'helpful-command)

;; Make it harder to kill emacs
(defun dont-kill-emacs ()
  "Disable `C-x `C-c' binding execute `kill-emacs'."
  (interactive)
  (error (substitute-command-keys "To exit emacs: \\[save-buffers-kill-frame-or-client]")))

(general-define-key
  "C-x C-c" 'dont-kill-emacs)

(provide 'init-key-bindings)
;;; init-key-bindings.el ends here
