;; init-package.el -- package management configs -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; straight

;;; Code:

;; Define package repositories

;; Bootstrap `straight`
(defun ensure-straight ()
  "Ensure `straight' is installed and compiled."
  (let* ((straight-file-name "straight/repos/straight.el/bootstrap.el")
         (straight-install-url "https://raw.githubusercontent.com/raxod502/straight.el/master/install.el")
         (bootstrap-file (expand-file-name straight-file-name user-emacs-directory)))
    (unless (file-exists-p bootstrap-file)
      (with-current-buffer
        (url-retrieve-synchronously
          straight-install-url
          'silent 'inhibit-cookies)
        (goto-char (point-max))
        (eval-print-last-sexp)))
    (load bootstrap-file nil 'nomessage)))

(ensure-straight)
(require 'straight)

(straight-use-package 'use-package)

;; Automatically install missing package
(setq straight-use-package-by-default t)

(eval-when-compile
  (require 'use-package))

(use-package use-package-ensure-system-package)

(provide 'init-package)
;;; init-package.el ends here
