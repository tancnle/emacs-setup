;; init-note-taking.el -- note-taking-related packages/configs -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; org-ref
;; ivy-bibtex

;;; Code:

(use-package org-ref
  :after org
  :init
  (setq
      org-ref-notes-directory "~/Dropbox/Notes/references/notes"
      org-ref-bibliography-notes "~/Dropbox/Notes/references/articles.org"
      org-ref-default-bibliography "~/Dropbox/Notes/references/articles.bib"
      org-ref-pdf-directory "~/Dropbox/Notes/references/pdfs/"
      org-ref-completion-library 'org-ref-ivy-cite))

(use-package ivy-bibtex
  :init
  ;; ivy-bibtex requires ivy's `ivy--regex-ignore-order` regex builder, which
  ;; ignores the order of regexp tokens when searching for matching candidates.
  (setq
    ivy-re-builders-alist '((ivy-bibtex . ivy--regex-ignore-order) (t . ivy--regex-plus))
    ;; Set paths for bibliography and notes the same as in org-ref
    bibtex-completion-bibliography org-ref-default-bibliography
    bibtex-completion-library-path org-ref-pdf-directory
    bibtex-completion-notes-path org-ref-bibliography-notes
    ;; Set action menu
    ivy-bibtex-default-action 'ivy-bibtex-edit-notes
    bibtex-completion-additional-search-fields '(tags)))


(provide 'init-note-taking)
;;; init-note-taking.el ends here
