;; init-misc.el -- miscellaneous packages/configs -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; Miscellaneous customizations.
;;
;; exec-path-from-shell
;; restclient
;; frame-cmds
;; frame-fns
;; zoom-frm

;;; Code:

;; Changes all yes/no questions to y/n type
(fset 'yes-or-no-p 'y-or-n-p)

;; shell scripts
(setq-default sh-basic-offset 2)
(setq-default sh-indentation 2)

;; No need for ~ files when editing
(setq create-lockfiles nil)

;; Go straight to scratch buffer on startup
(setq inhibit-startup-message t)

(setq initial-major-mode 'org-mode)
(setq initial-scratch-message nil)

(setq
 custom-file (expand-file-name "custom.el" user-emacs-directory)
 ;; reduce the frequency of garbage collection by making it happen on
 ;; each 50MB of allocated data (the default is on every 0.76MB)
 gc-cons-threshold 100000000

 ;; warn when opening files bigger than 100MB
 large-file-warning-threshold 100000000

 user-full-name "Tan Le"
 user-mail-address "tanle.oz@gmail.com")

(setq explicit-shell-file-name "/usr/local/bin/bash")
(setq shell-file-name "bash")

;; ignore TAGS re-read confirmation
(setq tags-revert-without-query t)

(use-package diminish)
(use-package delight)

(use-package exec-path-from-shell
  :config (exec-path-from-shell-initialize))

;; zoom-frm
(use-package frame-cmds
  :after frame-fns)

(use-package frame-fns)

(defun frame-maximize (&rest _)
  "Maximize frame on open."
  (set-frame-parameter nil 'fullscreen 'maximized))

(use-package zoom-frm
  :bind (("C-+" . zoom-frm-in)
         ("C--" . zoom-frm-out)
         ("C-0" . zoom-frm-unzoom)
         ("C-x C-+" . text-scale-increase)
         ("C-x C--" . text-scale-decrease)
         ("C-x C-0" . text-scale-adjust))
  :config
  (progn
    ;; Maximize frame after zooming
    (advice-add 'zoom-frm-in :after 'frame-maximize)
    (advice-add 'zoom-frm-out :after 'frame-maximize)
    (advice-add 'zoom-frm-unzoom :after 'frame-maximize)))

(use-package restclient)

(provide 'init-misc)
;;; init-misc.el ends here
