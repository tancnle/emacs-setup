;; init-go.el -- go-related packages/configs -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; company-go
;; go-mode

;;; Code:

(use-package company-go
  :defer t
  :init
  (progn
    (setq company-go-show-annotation t)
    (push 'company-go company-backends)))

(use-package go-mode
  :mode "\\.go\\'"
  :interpreter "go"
  :init
  (add-hook 'go-mode-hook (lambda () (setq-local tab-width 2))))

(provide 'init-go)
;;; init-go.el ends here
