;; init-js.el -- js-related packages/configs -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; css-mode
;; emmet-mode
;; js2-mode
;; js2-refactor
;; json-mode
;; json-reformat
;; json-snatcher
;; prettier-js
;; sgml-mode
;; tagedit
;; web-beautify
;; web-mode

;;; Code:

(use-package emmet-mode)

(use-package js2-mode
  :delight js2-mode (all-the-icons-alltheicon "javascript")
  :mode
  (("\\.js\\'" . js2-mode)
   ("\\.pac\\'" . js2-mode))
  :interpreter "node"
  :config
  (setq-default js-indent-level 2))

(use-package js2-refactor)

(use-package json-mode
  :defer t)

(use-package json-reformat)

(use-package json-snatcher)

(use-package css-mode
  :delight css-mode (all-the-icons-alltheicon "css3")
  :config
  (progn
    (setq-default css-indent-offset 2)
    (add-hook 'css-mode-hook #'emmet-mode)))

(use-package prettier-js)

;; javascript / html
(use-package sgml-mode
  :after (tagedit)
  :straight nil
  :delight html-mode (all-the-icons-alltheicon "html5")
  :config
  (progn
    (setq-default sgml-basic-offset 2)
    (add-hook 'html-mode-hook #'subword-mode)
    (add-hook 'html-mode-hook #'emmet-mode)

    (eval-after-load "sgml-mode"
      '(progn
         (require 'tagedit)
         (tagedit-add-paredit-like-keybindings)
         (add-hook 'html-mode-hook (lambda () (tagedit-mode 1)))))))

(use-package tagedit)

(use-package vue-mode)

(use-package web-beautify)

(use-package web-mode
  :delight web-mode (all-the-icons-icon-for-mode 'web-mode)
  :mode
  (("\\.phtml\\'" . web-mode)
   ("\\.tpl\\.php\\'" . web-mode)
   ("\\.[agj]sp\\'" . web-mode)
   ("\\.as[cp]x\\'" . web-mode)
   ("\\.erb\\'" . web-mode)
   ("\\.ejs\\'" . web-mode)
   ("\\.json\\'" . web-mode)
   ("\\.mustache\\'" . web-mode)
   ("\\.djhtml\\'" . web-mode))

  :config
  (progn
    (setq web-mode-markup-indent-offset 2)
    (setq web-mode-css-indent-offset 2)
    (setq web-mode-code-indent-offset 2)
    (setq web-mode-attr-indent-offset 2)))

;; react
(define-derived-mode react-mode web-mode "react")
(add-to-list 'auto-mode-alist '("\\.jsx\\'" . react-mode))
(add-to-list 'auto-mode-alist '("\\.react.js\\'" . react-mode))
(add-to-list 'auto-mode-alist '("\\index.android.js\\'" . react-mode))
(add-to-list 'auto-mode-alist '("\\index.ios.js\\'" . react-mode))
(add-to-list 'magic-mode-alist '("/\\*\\* @jsx React\\.DOM \\*/" . react-mode))
(add-to-list 'magic-mode-alist '("^import React" . react-mode))

(defun init-react-mode ()
  "Adjust web-mode to accommodate react-mode."
  (emmet-mode 0)
  ;; See https://github.com/CestDiego/emmet-mode/commit/3f2904196e856d31b9c95794d2682c4c7365db23
  (setq-local emmet-expand-jsx-className? t)
  ;; Force jsx content type
  (web-mode-set-content-type "jsx")
  ;; Don't auto-quote attribute values
  (setq-local web-mode-enable-auto-quoting nil)
  (setq mode-name (all-the-icons-alltheicon "react")))

(add-hook 'react-mode-hook #'init-react-mode)

(provide 'init-js)
;;; init-js.el ends here
