;; init-data-analysis.el -- data-analysis-related packages/configs -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; ess
;; ess-smart-equals
;; polymode
;; poly-markdown
;; poly-R
;; tex

;;; Code:

(use-package ess
  :mode ("\\.[rR]\\'" . R-mode)
  :config
  (progn
    (add-hook 'ess-mode-hook (lambda () (ess-set-style 'RStudio))) ; Make ESS use more horizontal screen
    (add-hook 'ess-R-post-run-hook 'ess-execute-screen-options)
    (setq ess-first-continued-statement-offset 2
          ess-continued-statement-offset 0
          ess-expression-offset 2
          ess-nuke-trailing-whitespace-p t
          ess-default-style 'DEFAULT)))

(use-package ess-smart-equals
  :init (setq ess-smart-equals-extra-ops '(brace paren percent))
  :after (:any ess-r-mode inferior-ess-r-mode ess-r-transcript-mode)
  :config (ess-smart-equals-activate))

(use-package polymode
  :requires (poly-R poly-markdown)
  :mode ("\\.Rmd\\'" . poly-markdown+r-mode))

(use-package poly-markdown)

(use-package poly-R)

(use-package tex
  :straight nil
  :ensure auctex
  :mode ("\\.tex\\'" . TeX-latex-mode)
  :config
  (progn
    (setq TeX-engine 'xetex)
    (setq TeX-PDF-mode t)
    (setq TeX-auto-save t)))

(provide 'init-data-analysis)
;;; init-data-analysis.el ends here
