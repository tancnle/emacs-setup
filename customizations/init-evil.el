;; init-evil.el -- evil-related packages/configs -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; evil
;; evil-collection
;; evil-matchit
;; evil-nerd-commenter
;; evil-surround
;; evil-visualstar

;;; Code:

;; Turn on evil (aka vim mode)
(use-package evil
  :custom
  (evil-want-keybinding nil)
  (evil-want-C-u-scroll t)
  (evil-want-fine-undo t)
  :config
  (evil-mode))

(use-package evil-collection
  :after evil
  :custom
  (evil-collection-company-use-tng nil)
  :config
  (evil-collection-init))

(use-package evil-escape
  :commands evil-escape
  :init
  (setq evil-escape-excluded-states '(normal visual multiedit emacs motion)
        evil-escape-excluded-major-modes '(neotree-mode treemacs-mode vterm-mode)
        evil-escape-key-sequence "jk"
        evil-escape-delay 0.15)
  (evil-define-key* '(insert replace visual operator) 'global "\C-g" #'evil-escape)
  :config
  ;; no `evil-escape' in minibuffer
  (add-hook 'evil-escape-inhibit-functions #'minibufferp)
  ;; so that evil-escape-mode-hook runs, and can be toggled by evil-mc
  (evil-escape-mode))

(use-package evil-matchit
  :hook (prog-mode . turn-on-evil-matchit-mode))

(use-package evil-nerd-commenter
  :commands (evilnc-comment-operator
             evilnc-inner-comment
             evilnc-outer-commenter))

(use-package evil-surround
  :hook (prog-mode . turn-on-evil-surround-mode))

(use-package evil-visualstar
  :commands (evil-visualstar/begin-search
             evil-visualstar/begin-search-forward
             evil-visualstar/begin-search-backward)
  :init
  (evil-define-key* 'visual 'global
                    "*" #'evil-visualstar/begin-search-forward
                    "#" #'evil-visualstar/begin-search-backward))

(provide 'init-evil)
;;; init-evil.el ends here
