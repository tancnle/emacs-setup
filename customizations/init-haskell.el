;; init-haskell.el -- haskell-related packages/configs -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; haskell-mode

;;; Code:

(use-package haskell-mode
  :bind (:map haskell-mode-map
              ("C-c C-l" . haskell-process-load-or-reload)
              ("C-`" . haskell-interactive-bring)
              ("C-c C-t" . haskell-process-do-type)
              ("C-c C-i" . haskell-process-do-info)
              ("C-c C-c" . haskell-process-cabal-build)
              ("C-c C-k" . haskell-interactive-mode-clear)
              ("C-c c" . haskell-process-cabal)))

(provide 'init-haskell)
;;; init-haskell.el ends here
