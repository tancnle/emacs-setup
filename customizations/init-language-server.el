;; init-language-server.el -- language-server-related packages/configs -*- lexical-binding: t; -*-
;;
;;; Commentary:

;; company-lsp
;; lsp-mode
;; lsp-ui
;; lsp-ivy

;;; Code:

(use-package company-lsp
  :after (company lsp)
  :commands company-lsp
  :custom
  (company-lsp-async t)
  (company-lsp-cache-candidates t)
  (company-lsp-enable-snippets nil)
  (company-lsp-enable-recompletion t)
  :config
  (add-to-list 'company-backends #'company-lsp))

(use-package lsp-mode
  :commands (lsp lsp-deferred)
  :hook (enh-ruby-mode . lsp-deferred)
  :custom
  (lsp-auto-configure t)
  (lsp-auto-guess-root t)

  ;; no real time syntax check
  (lsp-diagnostic-package :none)

  (lsp-enable-completion-at-point nil)

  ;; use `evil-matchit' instead
  (lsp-enable-folding nil)

  ;; use yasnippet instead
  (lsp-enable-snippet nil)

  ;; turn off for better performance
  (lsp-enable-symbol-highlighting nil)

  (lsp-file-watch-threshold 500)

  ;; enable log only for debug
  (lsp-log-io nil)
  (lsp-prefer-flymake nil)
  (lsp-print-performance t)
  (lsp-restart 'auto-restart)

  ;; ruby
  (lsp-solargraph-library-directories '("~/.rubies"))

  :config
  (add-to-list 'lsp-file-watch-ignored "[/\\\\][^/\\\\]*\\.\\(json\\|html\\|haml\\)$")
  (add-to-list 'lsp-file-watch-ignored "[/\\\\].git$")
  (add-to-list 'lsp-file-watch-ignored "node_modules$"))

(use-package lsp-ui
  :commands lsp-ui-mode
  :hook (lsp-mode-hook . lsp-ui-mode)
  :custom
  (lsp-prefer-flymake nil)
  (lsp-ui-sideline-enable t)
  (lsp-ui-flycheck-enable t)
  (lsp-ui-peek-enable t))

(use-package lsp-ivy
  :commands lsp-ivy-workspace-symbol)

(provide 'init-language-server)
;;; init-language-server.el ends here
