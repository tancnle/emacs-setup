;; init-scala.el -- scala-related packages/configs -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; ensime
;; sbt-mode
;; scala-mode

;;; Code:

(use-package ensime
  :commands (ensime ensime-mode)
  :after (scala-mode sbt-mode))

(use-package sbt-mode
  :defer t
  :after scala-mode)

(use-package scala-mode
  :delight scala-mode (all-the-icons-alltheicon "scala" :v-adjust -0.05))

(provide 'init-scala)
;;; init-scala.el ends here
