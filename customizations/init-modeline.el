;; init-modeline.el -- modeline-related packages/configs -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; doom-modeline

;;; Code:

(use-package doom-modeline
  :init (doom-modeline-mode 1))

(provide 'init-modeline)
;;; init-modeline.el ends here
