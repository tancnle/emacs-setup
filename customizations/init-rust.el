;; init-rust.el -- rust-related packages/configs -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; cargo
;; flycheck-rust
;; racer
;; rustic
;; toml-mode

;;; Code:

(use-package cargo
  :defer t)

(use-package flycheck-rust
  :defer t)

(use-package racer
  :hook (rustic-mode . racer-mode)
  :diminish racer-mode
  :config
  (progn
    (add-hook 'racer-mode-hook #'eldoc-mode)
    (add-hook 'racer-mode-hook #'company-mode)))

(use-package rustic
  :mode ("\\.rs$" . rustic-mode)
  :commands rustic-run-cargo-command rustic-cargo-outdated
  :config
  (setq rustic-indent-method-chain t
        rustic-flycheck-setup-mode-line-p nil
        rustic-lsp-client nil))

(use-package toml-mode)

(provide 'init-rust)
;;; init-rust.el ends here
