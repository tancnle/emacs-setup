;; init-elixir.el -- elixir-related packages/configs -*- lexical-binding: t; -*-
;;
;;; Commentary:

;; alchemist
;; elixir-mode

;;; Code:

(use-package alchemist)

(use-package elixir-mode)

(provide 'init-elixir)
;;; init-elixir.el ends here
