;; init-markdown.el -- markdown-related packages/configs -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; gh-md
;; markdow-mode
;; markdow-toc
;; vmd-mode

;;; Code:

(use-package gh-md)

(use-package markdown-mode
  :mode ("/README\\(?:\\.md\\)?\\'" . gfm-mode)
  :init
  (setq markdown-enable-math t
        markdown-enable-wiki-links t
        markdown-italic-underscore t
        markdown-asymmetric-header t
        markdown-fontify-code-blocks-natively t
        markdown-gfm-uppercase-checkbox t ; for compat with org-mode
        markdown-gfm-additional-languages '("sh")
        markdown-make-gfm-checkboxes-buttons t))

(use-package markdown-toc)

(use-package vmd-mode)

(provide 'init-markdown)
;;; init-markdown.el ends here
