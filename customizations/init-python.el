;; init-python.el -- python-related packages/configs -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; company-jedi
;; pytest
;; python

;;; Code:

(use-package company-jedi
  :defer t
  :config
  (eval-after-load "company"
    '(add-to-list 'company-backends 'company-jedi)))

(use-package pytest
  :commands (pytest-one
             pytest-pdb-one
             pytest-all
             pytest-pdb-all
             pytest-module
             pytest-pdb-module))

(use-package python
  :delight python-mode (all-the-icons-alltheicon "python")
  :interpreter ("python" . python-mode))

(provide 'init-python)
;;; init-python.el ends here
