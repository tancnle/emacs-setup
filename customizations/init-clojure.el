;; init-clojure.el -- clojure-related packages/configs -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; cider
;; clojure-mode
;; clj-refactor
;; clojure-mode-extra-font-locking
;; paredit

;;; Code:

(use-package cider
  :commands
  (cider-jack-in
   cider-jack-in-clojurescript)
  :init
  (progn
    (add-hook 'cider-mode-hook #'eldoc-mode)
    (add-hook 'cider-mode-hook #'company-mode)
    (add-hook 'cider-repl-mode-hook #'paredit-mode)
    (add-hook 'cider-repl-mode-hook #'eldoc-mode)
    (add-hook 'cider-repl-mode-hook #'rainbow-delimiters-mode)
    (add-hook 'cider-repl-mode-hook #'company-mode)

    ;; go right to the REPL buffer when it's finished connecting
    (setq cider-repl-pop-to-buffer-on-connect t)

    ;; When there's a cider error, show its buffer and switch to it
    (setq cider-show-error-buffer t)
    (setq cider-auto-select-error-buffer t)

    ;; Where to store the cider history.
    (setq cider-repl-history-file "~/.emacs.d/cider-history")

    ;; Wrap when navigating history.
    (setq cider-repl-wrap-history t))

  :config
  (progn
    (defun cider-start-http-server ()
      (interactive)
      (cider-load-current-buffer)
      (let ((ns (cider-current-ns)))
        (cider-repl-set-ns ns)
        (cider-interactive-eval (format "(println '(def server (%s/start))) (println 'server)" ns))
        (cider-interactive-eval (format "(def server (%s/start)) (println server)" ns))))

    (defun cider-refresh ()
      (interactive)
      (cider-interactive-eval (format "(user/reset)")))

    (defun cider-user-ns ()
      (interactive)
      (cider-repl-set-ns "user"))

    (eval-after-load 'cider
      '(progn
         (define-key clojure-mode-map (kbd "C-c C-v") 'cider-start-http-server)
         (define-key clojure-mode-map (kbd "C-M-r") 'cider-refresh)
         (define-key clojure-mode-map (kbd "C-c u") 'cider-user-ns)
         (define-key cider-mode-map (kbd "C-c u") 'cider-user-ns)))))

(use-package clojure-mode
  :commands (clojure-mode)
  :mode
  (("\\.edn$" . clojure-mode)
   ("\\.boot$" . clojure-mode)
   ("\\.cljs.*$" . clojurescript-mode)
   ("lein-env" . enh-ruby-mode))
  :init
  (delight 'clojure-mode (all-the-icons-alltheicon "clojure-line" :v-adjust -0.05) 'clojure-mode)
  (delight 'clojurescript-mode (all-the-icons-fileicon "cljs" :v-adjust -0.15) 'clojure-mode)
  :config
  (progn
    (add-hook 'clojure-mode-hook #'subword-mode)
    (add-hook 'clojure-mode-hook #'rainbow-delimiters-mode)
    (add-hook 'clojure-mode-hook #'enable-paredit-mode)

    ;; syntax hilighting for midje
    (add-hook 'clojure-mode-hook
              (lambda ()
                (setq inferior-lisp-program "lein repl")
                (font-lock-add-keywords
                 nil
                 '(("(\\(facts?\\)"
                    (1 font-lock-keyword-face))
                   ("(\\(background?\\)"
                    (1 font-lock-keyword-face))))
                (define-clojure-indent (fact 1))
                (define-clojure-indent (facts 1))))))

(use-package clj-refactor
  :after clojure-mode
  :diminish clj-refactor-mode
  :commands (clj-refactor-mode)
  :config
  (progn
    (cljr-add-keybindings-with-prefix "C-c C-m")
    (add-hook 'clojure-mode-hook #'clj-refactor-mode)))

(use-package clojure-mode-extra-font-locking
  :after clojure-mode)

(use-package paredit)

(provide 'init-clojure)

;;; init-clojure.el ends here
