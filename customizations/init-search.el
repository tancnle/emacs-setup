;; init-search.el -- search-related packages/configs -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; counsel
;; counsel-projectile
;; fasd
;; flx
;; ivy
;; ivy-rich
;; smex
;; swiper

;;; Code:

(defun selection-or-thing-at-point ()
  "Define a function to retrieve the selected string or symbol at the point if any."
  (cond
   ;; If there is selection use it
   ((and transient-mark-mode
         mark-active
         (not (eq (mark) (point))))
    (let ((mark-saved (mark))
          (point-saved (point)))
      (deactivate-mark)
      (buffer-substring-no-properties mark-saved point-saved)))
   ;; Otherwise, use symbol at point or empty
   (t (format "%s"
              (or (thing-at-point 'symbol)
                  "")))))

(use-package counsel
  :after smex
  :diminish counsel-mode
  :bind (:map counsel-mode-map
              ("M-x" . counsel-M-x)
              ("C-x C-f" . counsel-find-file))
  :init
  (require 'smex)
  :config
  (progn
    (counsel-mode 1)

    (setq counsel-rg-base-command "rg -i --no-heading --line-number --color never %s .")

    (defun counsel-rg-at-point ()
      "counsel-rg with at-point and project root enhancement
The initial string is produced by selection-or-thing-at-point.
The directory is detected by projectile-project-root."
      (interactive)
      (counsel-rg (selection-or-thing-at-point)
                  ;; This will be replaced by the above advice anyway.
                  (projectile-project-root)))))

(use-package counsel-etags
  :init
  (add-hook 'prog-mode-hook
        (lambda ()
          (add-hook 'after-save-hook
            'counsel-etags-virtual-update-tags 'append 'local)))
  :custom
  (counsel-etags-update-interval 60)
  :config
  (push "build" counsel-etags-ignore-directories))

(use-package counsel-projectile
  :after projectile
  :defer t
  :custom
  (counsel-find-file-ignore-regexp "\\.po\\'")
  (projectile-completion-system 'ivy)
  :config
  (counsel-projectile-mode))

(use-package fasd
  :config
  (progn
    (setq fasd-completing-read-function 'ivy-completing-read)
    (global-set-key (kbd "C-h C-/") 'fasd-find-file)
    (global-fasd-mode 1)))

(use-package flx
  :defer t
  :custom
  (ivy-flx-limit 10000))

(use-package ivy
  :diminish ivy-mode
  :bind (:map ivy-mode-map
              ("C-x b" . ivy-switch-buffer)
              ("<f4>"  . ivy-resume))
  :custom
  (ivy-initial-inputs-alist nil)
  (ivy-use-virtual-buffers t)
  (ivy-count-format "(%d/%d) ")
  (ivy-re-builders-alist
   '((swiper             . ivy--regex-plus)
     (counsel-rg         . ivy--regex-plus)
     (counsel-projectile . ivy--regex-fuzzy)
     (t                  . ivy--regex-fuzzy)))
  :config
  (ivy-mode 1))

(use-package ivy-rich
  :after ivy
  :config
  (ivy-rich-mode))

(use-package rg
  :if (executable-find "rg")
  :commands (rg)
  :config
  (setq rg-command-line-flags '())
  (setq rg-group-result t))

(use-package smex
  :custom
  (smex-save-file (concat user-emacs-directory ".smex-items"))
  :config
  (smex-initialize))

(use-package swiper
  :commands (swiper
             swiper-all
             swiper-at-point
             swiper-all-at-point)
  :bind (("C-s" . swiper-at-point)
         ("M-I" . swiper-all-at-point))
  :config
  (defun swiper-at-point ()
    "Custom function to pick up a thing at a point for swiper
If a selected region exists, it will be searched for by swiper
If there is a symbol at the current point, its textual representation is
searched. If there is no symbol, empty search box is started."
    (interactive)
    (swiper (selection-or-thing-at-point)))

  (defun swiper-all-at-point ()
    "Custom function to pick up a thing at a point for swiper
If a selected region exists, it will be searched for by swiper
If there is a symbol at the current point, its textual representation is
searched. If there is no symbol, empty search box is started."
    (interactive)
    (swiper-all (selection-or-thing-at-point))))

(defun create-tags (dir-name)
  "Create tags file, asks for DIR-NAME."
  (interactive "DDirectory: ")
  (let ((path-to-ctags "/usr/local/bin/ctags"))
    (shell-command
     (format
      "%s -f %s/tags -eR --exclude=coverage %s"
      path-to-ctags
      (directory-file-name dir-name)
      (directory-file-name dir-name)))))

(provide 'init-search)
;;; init-search.el ends here
