;; init-org.el -- org-related packages/configs -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; Customizations relating to org-mode
;;
;; evil-org
;; ob-mermaid
;; org
;; org-bullets

;;; Code:

(use-package evil-org
  :hook (org-mode . evil-org-mode)
  :config
  (evil-org-set-key-theme)
  (require 'evil-org-agenda)
  (evil-org-agenda-set-keys))

(use-package ob-mermaid
  :straight (:host github :repo "arnm/ob-mermaid" :branch "master"))

(use-package org
  :defer t
  :config
  (progn
    (setq org-export-coding-system 'utf8)
    (setq org-todo-keywords
          '((sequence
             "TODO(t)"
             "WAITING(w!)"
             "INPROGRESS(i!)"
             "|"
             "DONE(d!)"
             "CANCELLED(c!)")))
    (setq org-todo-keyword-faces
          '(("TODO" . (:foreground "#ffa724" :weight bold))
            ("WAITING" . (:foreground "#d7d7ff"))
            ("INPROGRESS" . (:foreground "#0a9dff"))
            ("DONE" . (:foreground "#aeee00"))
            ("CANCELLED" . (:foreground "#b88853"))))

    (setq org-blank-before-new-entry '((heading . t)
                                       (plain-list-item . t)))
    (setq org-capture-templates
          `(("t" "Todo" entry (file "inbox.org")
             (file "tpl-todo.txt")
             :prepend t
             :empty-lines 1)

            ("j" "Journal" entry (file+datetree "journal.org")
             (file "tpl-journal.txt")
             :prepend t
             :empty-lines 1)

            ("b" "Tidbit: quote, link, snippet" entry (file "tidbit.org")
             "* %?\n"
             :prepend t
             :empty-lines 1)

            ("l" "Something to read later" entry (file "reading.org")
             (file "tpl-reading.txt")
             :prepend t
             :empty-lines 1)))

    (setq org-default-notes-file "~/Dropbox/Notes/inbox.org")
    (setq org-directory "~/Dropbox/Notes")

    ;; Logging of state changes
    (setq org-log-done (quote time))
    (setq org-log-redeadline (quote time))
    (setq org-log-reschedule (quote time))
    (setq org-log-into-drawer t)

    (setq org-use-sub-superscripts '{})
    (setq org-pretty-entities t)
    (setq org-insert-heading-respect-content t)
    (setq org-ellipsis " ▼ ")
    (setq org-export-initial-scope 'subtree)
    (setq org-use-tag-inheritance nil) ;; Use the list form, which happens to be blank

    ;; Agenda configuration
    (setq org-agenda-text-search-extra-files '(agenda-archives))
    (setq org-agenda-files (list org-directory))

    (setq org-refile-targets
          '((nil :maxlevel . 3)
            (org-agenda-files :maxlevel . 3)))

    (setq org-refile-use-outline-path 'file)
    (setq org-refile-allow-creating-parent-nodes 'confirm)
    (setq org-outline-path-complete-in-steps nil)
    (setq org-agenda-time-grid '((daily today require-timed)
                                 ""
                                 (800 1000 1200 1400 1600)))
    (setq org-enforce-todo-dependencies t)
    (setq org-agenda-dim-blocked-tasks t)

    (setq org-src-preserve-indentation nil)
    (setq org-edit-src-content-indentation 0)

    ;; Language support
    (setq org-ditaa-jar-path "~/bin/ditaa.jar")
    (setq org-plantuml-jar-path "~/bin/plantuml.jar")
    (setq ob-mermaid-cli-path "/usr/local/bin/mmdc")

    (require 'ob-clojure)
    (setq org-babel-clojure-backend 'cider)
    (require 'cider)

    (org-babel-do-load-languages
     'org-babel-load-languages
     '((ditaa . t)
       (mermaid . t)
       (python . t)
       (ruby . t)
       (clojure . t)
       (haskell . t)
       (js . t)
       (shell . t)
       (sql . t)
       (plantuml . t)
       (latex . t)))

    (setq org-src-fontify-natively t
          org-confirm-babel-evaluate nil)

    (defun display-inline-images ()
      (condition-case nil
          (org-display-inline-images)
        (error nil)))

    (add-hook 'org-babel-after-execute-hook 'org-redisplay-inline-images)

    ;; Make windmove work in org-mode
    (add-hook 'org-shiftup-final-hook 'windmove-up)
    (add-hook 'org-shiftleft-final-hook 'windmove-left)
    (add-hook 'org-shiftdown-final-hook 'windmove-down)
    (add-hook 'org-shiftright-final-hook 'windmove-right)

    (add-hook 'org-capture-mode-hook 'evil-insert-state)))

(use-package org-bullets
  :hook (org-mode . org-bullets-mode))

(provide 'init-org)
;;; init-org.el ends here
