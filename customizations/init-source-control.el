;; init-source-control.el -- source-control-related packages/configs -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; Customizations relating to source-control.
;;
;; diff-hl
;; git-link
;; magit

;;; Code:

(use-package diff-hl
  :hook (dired-mode . diff-hl-dired-mode)
  :hook (magit-post-refresh . diff-hl-magit-post-refresh)
  :config
  ;; use margin instead of fringe
  (diff-hl-margin-mode)
  (global-diff-hl-mode))

(use-package git-link
  :custom
  (git-link-use-commit t))

(use-package magit
  :delight magit-mode (all-the-icons-alltheicon "git")
  :hook (git-commit-setup . git-commit-turn-on-flyspell)
  :hook (git-commit-setup . git-commit-turn-on-auto-fill)
  :custom
  (magit-refresh-status-buffer nil)
  (git-commit-summary-max-length 50)
  (git-commit-fill-column 72))

(provide 'init-source-control)
;;; init-source-control.el ends here
