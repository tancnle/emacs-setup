;; init-ruby.el -- ruby-related packages/configs -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; bundler
;; enh-ruby-mode
;; goto-gem
;; haml-mode
;; inf-ruby
;; chruby
;; minitest
;; rake
;; robe
;; rspec-mode
;; rubocop
;; ruby-hash-syntax
;; ruby-refactor
;; ruby-test-mode
;; ruby-tools
;; yaml-mode
;; yard-mode

;;; Code:

(use-package bundler)

(use-package enh-ruby-mode
  :delight enh-ruby-mode (all-the-icons-octicon "ruby")
  :mode (("Appraisals\\'" . enh-ruby-mode)
         ("\\(Rake\\|Thor\\|Guard\\|Gem\\|Cap\\|Vagrant\\|Berks\\|Pod\\|Puppet\\)file\\'" . enh-ruby-mode)
         ("\\.\\(rb\\|rabl\\|ru\\|builder\\|rake\\|thor\\|gemspec\\|jbuilder\\)\\'" . enh-ruby-mode))
  :interpreter "ruby"
  :init
  (progn
    (setq enh-ruby-add-encoding-comment-on-save nil
          enh-ruby-deep-indent-paren nil
          enh-ruby-hanging-paren-deep-indent-level 2
          enh-ruby-program (shell-command-to-string "printf %s \"$(which ruby)\"")))
  :ensure-system-package
  ((ruby-lint   . "gem install ruby-lint")
   (reek        . "gem install reek")
   (ripper-tags . "gem install ripper-tags")
   (pry         . "gem install pry")
   (solargraph  . "gem install solargraph")))

(use-package goto-gem
  :defer t)

(use-package haml-mode
  :defer t)

(use-package inf-ruby
  :hook (enh-ruby-mode . inf-ruby-minor-mode))

(use-package chruby
  :commands chruby-use-corresponding
  :hook (enh-ruby-mode . chruby-use-corresponding))

(use-package minitest)

(use-package rake)

(use-package robe
  :diminish robe-mode
  :hook (enh-ruby-mode . robe-mode)
  :config
  (eval-after-load 'company
    '(push 'company-robe company-backends)))

(defun inf-ruby-auto-enter ()
  "Automatically enters inf-ruby-mode in ruby modes' debugger breakpoints."
  (add-hook 'compilation-filter-hook 'inf-ruby-auto-enter nil t))

(use-package rspec-mode
  :hook enh-ruby-mode
  :diminish rspec-mode
  :config
  (progn
    (add-hook 'rspec-compilation-mode-hook #'inf-ruby-auto-enter)
    (setq rspec-use-rake-when-possible nil
          compilation-scroll-output 'first-error)))

(use-package rubocop
  :hook (enh-ruby-mode . rubocop-mode)
  :diminish rubocop-mode)

(use-package ruby-hash-syntax
  :defer t)

(use-package ruby-refactor
  :hook (enh-ruby-mode . ruby-refactor-mode-launch))

(use-package ruby-test-mode)

(use-package ruby-tools
  :hook (enh-ruby-mode . ruby-tools-mode)
  :diminish ruby-tools-mode)

(use-package yaml-mode
  :defer t)

(use-package yard-mode
  :hook enh-ruby-mode)

(provide 'init-ruby)
;;; init-ruby.el ends here
