;; init-editing.el -- editing-related packages/configs -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; Customizations relating to editing a buffer.
;;
;; abbrev
;; avy
;; cheat-sh
;; company
;; company-box
;; company-ctags
;; dockerfile-mode
;; eldoc
;; fill-column-indicator
;; flycheck
;; helpful
;; rainbow-delimiters
;; rainbow-mode
;; saveplace
;; subword
;; super-save
;; undo-tree
;; vimrc-mode
;; whitespace
;; yafolding
;; yasnippet
;; yasnippet-snippets

;;; Code:

(setq delete-by-moving-by-moving-to-trash t)

;; revert buffers automatically when underlying files are changed externally
(global-auto-revert-mode 1)
(setq
  global-auto-revert-non-file-buffers t
  auto-revert-verbose nil)
(add-to-list 'global-auto-revert-ignore-modes 'Buffer-menu-mode)

;; Highlights matching parenthesis
(show-paren-mode 1)

;; Highlight current line
(global-hl-line-mode 1)

;; Don't use hard tabs
(setq-default indent-tabs-mode nil)

;; Emacs can automatically create backup files. This tells Emacs to
;; put all backups in ~/.emacs.d/backups. More info:
;; http://www.gnu.org/software/emacs/manual/html_node/elisp/Backup-Files.html
(setq backup-directory-alist `(("." . ,(concat user-emacs-directory
                                               "backups"))))
(setq auto-save-default nil)

(setq-default fill-column 120)

(setq require-final-newline t)

(defun die-tabs ()
  "Use 2 spaces for tabs."
  (interactive)
  (set-variable 'tab-width 2)
  (mark-whole-buffer)
  (untabify (region-beginning) (region-end))
  (keyboard-quit))

;; fix weird os x kill error
(defun ns-get-pasteboard ()
  "Return the value of the pasteboard, or nil for unsupported formats."
  (condition-case nil
      (ns-get-selection-internal 'CLIPBOARD)
    (quit nil)))

(setq electric-indent-mode nil)

(define-key global-map (kbd "RET") 'newline-and-indent)

(use-package abbrev
  :straight nil
  :diminish abbrev-mode
  :config
  (setq save-abbrevs 'silently))

(use-package adaptive-wrap)

(use-package avy
  :bind
  (("M-g c" . avy-goto-char)
   ("M-g l" . avy-goto-line)
   ("M-g w" . avy-goto-word-1))
  :config
  (setq avy-background t))

(use-package cheat-sh)

(defvar-local company-fci-mode-on-p nil)

(defun company-turn-off-fci (&rest ignore)
  "Turn off fci on company completion."
  (when (boundp 'fci-mode)
    (setq company-fci-mode-on-p fci-mode)
    (when fci-mode (fci-mode -1))))

(defun company-maybe-turn-on-fci (&rest ignore)
  "Turn on fci on company completion."
  (when company-fci-mode-on-p (fci-mode 1)))

(use-package company
  :diminish company-mode "ⓐ"
  :hook (prog-mode . company-mode)
  :init
  (progn
    (add-hook 'company-completion-started-hook 'company-turn-off-fci)
    (add-hook 'company-completion-finished-hook 'company-maybe-turn-on-fci)
    (add-hook 'company-completion-cancelled-hook 'company-maybe-turn-on-fci))
  :config
  (progn
    (require 'company-dabbrev)
    (require 'company-dabbrev-code)
    (setq
     company-dabbrev-ignore-case nil
     company-dabbrev-code-ignore-case nil
     company-dabbrev-downcase nil
     company-idle-delay 0
     company-show-numbers t
     company-minimum-prefix-length 3
     company-tooltip-align-annotations t
     company-tooltip-limit 14
     company-require-match 'never
     company-backends '((company-capf :with company-dabbrev-code)
                        company-dabbrev
                        company-dabbrev-code
                        company-files
                        company-keywords))))

(use-package company-box
  :diminish company-box-mode
  :hook (company-mode . company-box-mode)
  :config
  (setq company-box-show-single-candidate t
        company-box-backends-colors nil
        company-box-max-candidates 50
        company-box-icons-alist 'company-box-icons-all-the-icons
        company-box-icons-all-the-icons
        `((Unknown       . ,(all-the-icons-material "find_in_page"             :height 0.8 :face 'all-the-icons-purple))
          (Text          . ,(all-the-icons-material "text_fields"              :height 0.8 :face 'all-the-icons-green))
          (Method        . ,(all-the-icons-material "functions"                :height 0.8 :face 'all-the-icons-red))
          (Function      . ,(all-the-icons-material "functions"                :height 0.8 :face 'all-the-icons-red))
          (Constructor   . ,(all-the-icons-material "functions"                :height 0.8 :face 'all-the-icons-red))
          (Field         . ,(all-the-icons-material "functions"                :height 0.8 :face 'all-the-icons-red))
          (Variable      . ,(all-the-icons-material "adjust"                   :height 0.8 :face 'all-the-icons-blue))
          (Class         . ,(all-the-icons-material "class"                    :height 0.8 :face 'all-the-icons-red))
          (Interface     . ,(all-the-icons-material "settings_input_component" :height 0.8 :face 'all-the-icons-red))
          (Module        . ,(all-the-icons-material "view_module"              :height 0.8 :face 'all-the-icons-red))
          (Property      . ,(all-the-icons-material "settings"                 :height 0.8 :face 'all-the-icons-red))
          (Unit          . ,(all-the-icons-material "straighten"               :height 0.8 :face 'all-the-icons-red))
          (Value         . ,(all-the-icons-material "filter_1"                 :height 0.8 :face 'all-the-icons-red))
          (Enum          . ,(all-the-icons-material "plus_one"                 :height 0.8 :face 'all-the-icons-red))
          (Keyword       . ,(all-the-icons-material "filter_center_focus"      :height 0.8 :face 'all-the-icons-red))
          (Snippet       . ,(all-the-icons-material "short_text"               :height 0.8 :face 'all-the-icons-red))
          (Color         . ,(all-the-icons-material "color_lens"               :height 0.8 :face 'all-the-icons-red))
          (File          . ,(all-the-icons-material "insert_drive_file"        :height 0.8 :face 'all-the-icons-red))
          (Reference     . ,(all-the-icons-material "collections_bookmark"     :height 0.8 :face 'all-the-icons-red))
          (Folder        . ,(all-the-icons-material "folder"                   :height 0.8 :face 'all-the-icons-red))
          (EnumMember    . ,(all-the-icons-material "people"                   :height 0.8 :face 'all-the-icons-red))
          (Constant      . ,(all-the-icons-material "pause_circle_filled"      :height 0.8 :face 'all-the-icons-red))
          (Struct        . ,(all-the-icons-material "streetview"               :height 0.8 :face 'all-the-icons-red))
          (Event         . ,(all-the-icons-material "event"                    :height 0.8 :face 'all-the-icons-red))
          (Operator      . ,(all-the-icons-material "control_point"            :height 0.8 :face 'all-the-icons-red))
          (TypeParameter . ,(all-the-icons-material "class"                    :height 0.8 :face 'all-the-icons-red))
          ;; (Template   . ,(company-box-icons-image "Template.png"))))
          (Yasnippet     . ,(all-the-icons-material "short_text"               :height 0.8 :face 'all-the-icons-green))
          (ElispFunction . ,(all-the-icons-material "functions"                :height 0.8 :face 'all-the-icons-red))
          (ElispVariable . ,(all-the-icons-material "check_circle"             :height 0.8 :face 'all-the-icons-blue))
          (ElispFeature  . ,(all-the-icons-material "stars"                    :height 0.8 :face 'all-the-icons-orange))
          (ElispFace     . ,(all-the-icons-material "format_paint"             :height 0.8 :face 'all-the-icons-pink)))))

;; (use-package company-ctags)

(use-package dockerfile-mode
  :defer t
  :delight dockerfile-mode (all-the-icons-fileicon "dockerfile"))

;; eldoc-mode shows documentation in the minibuffer when writing code
(use-package eldoc
  :hook ((enh-ruby-mode ielm-mode) . eldoc-mode)
  :diminish eldoc-mode)

(use-package fill-column-indicator
  :hook (prog-mode . fci-mode)
  :config
  (setq
   fci-rule-width 1
   fci-rule-color "darkblue"))

(use-package flycheck
  :hook ((prog-mode markdown-mode) . flycheck-mode)
  :diminish flycheck-mode "✈"
  :config
  (progn
    (setq-default flycheck-disabled-checkers
                  (append flycheck-disabled-checkers
                          '(ruby-rubylint)))

    ;; use eslint with web-mode for jsx files
    (flycheck-add-mode 'javascript-eslint 'web-mode)))

(use-package helpful)

(use-package ivy-yasnippet)

(use-package highlight-numbers
  :hook (prog-mode . highlight-numbers-mode))

(use-package highlight-operators
  :hook (prog-mode . highlight-operators-mode))

(use-package highlight-escape-sequences
  :hook (prog-mode . hes-mode))

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package rainbow-mode
  :hook prog-mode
  :diminish rainbow-mode)

(use-package persistent-scratch)

;; saveplace remembers your location in a file when saving files
(use-package saveplace
  :config
  ;; keep track of saved places in ~/.emacs.d/places
  (setq save-place-file '(concat user-emacs-directry "places"))
  ;; activate it for all buffers
  (setq-default save-place t))

(use-package subword
  :diminish subword-mode)

;; save emacs buffers when they lose focus
(use-package super-save
  :diminish super-save-mode
  :config
  (super-save-mode +1))

(use-package undo-tree
  :diminish undo-tree-mode
  :hook (prog-mode . undo-tree-mode))

(use-package vimrc-mode)

(use-package whitespace
  :diminish whitespace-mode
  :hook ((prog-mode . whitespace-mode)
         (before-save . whitespace-cleanup))
  :config
  (progn
    (setq whitespace-line-column 120)
    (setq whitespace-style '(face tabs empty trailing lines-tail))))

(use-package yafolding
  :hook (prog-mode . yafolding-mode))

(use-package yasnippet
  :commands (yas-reload-all yas-minor-mode)
  :hook ((prog-mode org-mode) . yas-minor-mode)
  :config
  (yas-reload-all))

(use-package yasnippet-snippets
  :after yasnippet)

(provide 'init-editing)
;;; init-editing.el ends here
