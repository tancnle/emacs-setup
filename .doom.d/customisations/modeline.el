;; modeline.el -- modeline-related packages/configs -*- lexical-binding: t; -*-

;; Use orange instead of red text in the modeline
(custom-set-faces!
  '(doom-modeline-buffer-modified :foreground "orange"))

(display-time-mode 1)                              ; Enable time in the mode-line

(unless (string-match-p "^Power N/A" (battery))    ; On laptops...
  (display-battery-mode 1))                        ; it's nice to know how much power you have

;; LF UTF-8 is the default file encoding, and thus not worth noting in the modeline.
;; So, let’s conditionally hide it.
(defun doom-modeline-conditional-buffer-encoding ()
  "We expect the encoding to be LF UTF-8, so only show the modeline when this is not the case"
  (setq-local doom-modeline-buffer-encoding
    (unless (and (memq (plist-get (coding-system-plist buffer-file-coding-system) :category)
                   '(coding-category-undecided coding-category-utf-8))
              (not (memq (coding-system-eol-type buffer-file-coding-system) '(1 2))))
      t)))

(add-hook 'after-change-major-mode-hook #'doom-modeline-conditional-buffer-encoding)

(provide 'modeline)
;;; modeline.el ends here
