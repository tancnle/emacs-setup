;; org.el -- org-related packages/configs -*- lexical-binding: t; -*-

(use-package! ob-mermaid
  :init
  (after! org-src
    (add-to-list 'org-src-lang-modes '("mermaid" . mermaid))))

(use-package! org
  :config
  (progn
    (setq org-capture-templates
      `(("t" "Todo" entry (file "inbox.org")
          (file "tpl-todo.txt")
          :prepend t
          :empty-lines 1)

         ("j" "Journal" entry (file+datetree "journal.org")
           (file "tpl-journal.txt")
           :prepend t
           :empty-lines 1)

         ("b" "Tidbit: quote, link, snippet" entry (file "tidbit.org")
           "* %?\n"
           :prepend t
           :empty-lines 1)

         ("l" "Something to read later" entry (file "reading.org")
           (file "tpl-reading.txt")
           :prepend t
           :empty-lines 1)))
    ))

(use-package! org-bullets
  :hook (org-mode . org-bullets-mode))

;;; org.el ends here
