;; evil.el -- evil-related packages/configs -*- lexical-binding: t; -*-

(use-package! evil
  :custom
  (evil-want-fine-undo t))

(after! evil
  ;; Split and focus on the new window
  (setq evil-split-window-below t
    evil-vsplit-window-right t)

  ;; Prompt for which buffer to open
  (defadvice! prompt-for-buffer (&rest _)
    :after '(evil-window-split evil-window-vsplit)
    (consult-buffer)))

;; Remove `evil-' prefix
(setq which-key-allow-multiple-replacements t)
(after! which-key
  (pushnew!
   which-key-replacement-alist
   '(("" . "\\`+?evil[-:]?\\(?:a-\\)?\\(.*\\)") . (nil . "◂\\1"))
   '(("\\`g s" . "\\`evilem--?motion-\\(.*\\)") . (nil . "◃\\1"))
   ))

;; evil.el ends here
